#!/bin/bash

DATABASE_NAME=db0
INSERTER_NAME=mongo_inserter

# Start the daemon itself
docker run --name $DATABASE_NAME -d mongo

# Load in the data
# Create the loader container
docker create --link $DATABASE_NAME:mongo --name $INSERTER_NAME -it mongo
# .. and start it
docker start $INSERTER_NAME
# Copy our dataset to it
docker exec -i $INSERTER_NAME /bin/bash -c 'cat > /root/dataset.json' < dataset.json
# And inject the dataset using mongoimport
docker exec -it $INSERTER_NAME sh -c 'exec mongoimport --host $MONGO_PORT_27017_TCP_ADDR:$MONGO_PORT_27017_TCP_PORT -d cave -c rooms --drop --file /root/dataset.json'
# Stop the inserter
docker stop $INSERTER_NAME
# ... and remove it
docker rm $INSERTER_NAME
