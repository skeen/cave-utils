#!/bin/bash

echoerr() { echo "$@" 1>&2; }

#echo $1
#echo $2

daemon_reply_expected="{\"error-message\":\"JSON Parse error on input = GET \/ HTTP\/1.1\",\"error-code\":\"GENERAL_SERVER_FAILURE\",\"version\":\"2\"}"
daemon_reply=$(curl -m 5 -s http://$1:37123/)
if [ "$daemon_reply" = "$daemon_reply_expected" ]; then
    echoerr "$3 daemon is up"
else
    echoerr "$3 daemon is down"
fi




