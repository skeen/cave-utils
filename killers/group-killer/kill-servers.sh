#!/bin/bash

echoerr() { echo "$@" 1>&2; }

while IFS='' read -r line || [[ -n "$line" ]]; do

    CLEAN=$(echo $line | sed "s/ (dot) /\./g" | sed "s/^.*:\s//g")
    GROUP=$(echo $line | sed "s/\(.*\):\s.*/\1/g")
    HOSTNAME=$(echo $CLEAN | sed "s/\(.*\):.*/\1/g")
    PORT=$(echo $CLEAN | sed "s/.*:\(.*\)/\1/g")
    echo "Group: $GROUP"
    echo "Hostname: $HOSTNAME"
    echo "PORT: $PORT"
    
    IP=$(dig +short $HOSTNAME | tail -1)
    if [ -z "$IP" ]; then
        IP=$HOSTNAME
    fi
    echo "IP: $IP"
    echo ""

    echo "Firing at $IP:$PORT"
    ./fire.sh $IP $PORT "$GROUP" 1>/dev/null 2> /dev/null
    echo "Telnet done!"
    ./test.sh $IP $PORT "$GROUP"
    echo "Curl done!"
    echo ""
done < "group_ip.txt"
