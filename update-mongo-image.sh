#!/bin/bash

echo "Pull the new release"
docker pull skeen/staging-socket:latest

echo "Stopping service"
service docker-daemon stop

echo "Stopping docker daemon"
docker stop daemon

echo "Removing docker daemon"
docker rm daemon

echo "Starting new daemon"
docker run -d --link db0:db0 -p 37123:37123 --name daemon skeen/staging-socket:latest bash /root/cave/start-mongo.sh

echo "Enabling service"
service docker-daemon start
