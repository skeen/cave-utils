#!/bin/bash

DOCKER_PS=$(docker ps -a | grep "daemon")
CONTAINER_NAME=daemon
IMAGE_PATH=skeen/staging-socket:latest

echoerr() { echo "$@" 1>&2; }

start() {
  if [ "$DOCKER_PS" ]; then
    docker start -a $CONTAINER_NAME
  else
    echo "No container found, building one!"
    pull_image
    docker start -a $CONTAINER_NAME
  fi
}

pull_image() {
  docker pull $IMAGE_PATH
  if [ "$DOCKER_PS" ]; then
    docker stop -t 5 $CONTAINER_NAME
    docker rm $CONTAINER_NAME
  fi
  docker create -p 37123:37123 --name $CONTAINER_NAME $IMAGE_PATH bash /root/cave/start-socket.sh
}

update_image() {
    service docker-daemon stop
    pull_image
    service docker-daemon start
}

stop() {
  if [ "$DOCKER_PS" ]; then
    docker stop -t 5 $CONTAINER_NAME
  else
    echoerr "Error: No daemon container running"
  fi
}

case $1 in
  start|update_image|stop) "$1" ;;
esac
