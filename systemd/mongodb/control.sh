#!/bin/bash

DOCKER_PS=$(docker ps -a | grep "db0")
CONTAINER_NAME=db0
IMAGE_PATH=mongo:latest

echoerr() { echo "$@" 1>&2; }

start() {
  if [ "$DOCKER_PS" ]; then
    docker start -a $CONTAINER_NAME
  else
    echo "No container found, building one!"
    pull_image
    docker start -a $CONTAINER_NAME
  fi
}

stop() {
  if [ "$DOCKER_PS" ]; then
    docker stop -t 5 $CONTAINER_NAME
  else
    echoerr "Error: No daemon container running"
  fi
}

case $1 in
  start|stop) "$1" ;;
esac
