#!/bin/bash

SERVICE_FILE=/etc/systemd/system/docker-daemon.service
#SERVICE_FILE=a.service
SCRIPT_DIR=$(cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd)
SCRIPT_FILE="$SCRIPT_DIR/control.sh"

#echo "$SCRIPT_FILE"

echo -e "[Unit]\nDescription=Docker daemon container\nRequires=docker.service\nAfter=docker.service\n" > $SERVICE_FILE
echo -e "[Service]\nRestart=always\nExecStart=$SCRIPT_FILE start\nExecStop=$SCRIPT_FILE stop\n" >> $SERVICE_FILE
echo -e "[Install]\nWantedBy=local.target" >> $SERVICE_FILE

systemctl daemon-reload
