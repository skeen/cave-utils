#!/bin/bash

gource --output-custom-log log1.txt wiki/
gource --output-custom-log log2.txt cave-dev/
gource --output-custom-log log3.txt cave-cgi/
gource --output-custom-log log4.txt cave-utils/
sed -i -r "s#(.+)\|#\1|/repo1#" log1.txt
sed -i -r "s#(.+)\|#\1|/repo2#" log2.txt
sed -i -r "s#(.+)\|#\1|/repo2#" log3.txt
sed -i -r "s#(.+)\|#\1|/repo2#" log4.txt
cat log1.txt log2.txt log3.txt log4.txt | sort -n > combined.txt
gource combined.txt

