#!/bin/bash

cd /root/cave-utils/score-checker/

LOG="/tmp/score_up.err"

curl -s "http://users-cs.au.dk/baerbak/c/cloud/current-score-operations-socket.txt" | grep "CSS 13" | sed 's/CSS 13\s*//g' | sed 's/\s*[0-9][0-9]*\s*$//g' > new_score.log
rm -f $LOG

if [ ! -f old_score.log ]; then
	cp new_score.log old_score.log
fi

NEW_TRIALS_COUNT=$(sed 's/\([0-9][0-9]*\)\s*\([0-9][0-9]*\)/\1/g' < new_score.log)
OLD_TRIALS_COUNT=$(sed 's/\([0-9][0-9]*\)\s*\([0-9][0-9]*\)/\1/g' < old_score.log)

NEW_SCORE=$(sed 's/\([0-9][0-9]*\)\s*\([0-9][0-9]*\)/\2/g' < new_score.log)
OLD_SCORE=$(sed 's/\([0-9][0-9]*\)\s*\([0-9][0-9]*\)/\2/g' < old_score.log)

if [ "$NEW_TRIALS_COUNT" -gt "$OLD_TRIALS_COUNT" ]; then
	echo "New trial found"
	if [ "$NEW_SCORE" -le "$OLD_SCORE" ]; then
		echo "Missing score!"
		echo "Missing score!" > $LOG
	else
		echo "Score is fine"
	fi
else
	echo "No new trials found"
fi

cp new_score.log old_score.log

if [[ -s $LOG ]] ; then
    cat mailheader.txt $LOG | /usr/sbin/sendmail sovende@gmail.com
fi
