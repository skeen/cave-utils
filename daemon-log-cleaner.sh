#!/bin/bash

# Generate log as; docker logs daemon > LOG_NAME.log

cat LOG_NAME.log | sed 's/\s*\[java\] --> Accepting...//g' | sed 's/\s*\[java\] --> AcceptED!//g' | sed 's/\s*\[java\] --> Received null//g' | sed 's/\s*\[java\] Closing socket...//g' | sed 's/\s* \[java\] --< !!! replied: {"error-message":"Unknown error while parsing JSON","error-code":"GENERAL_SERVER_FAILURE","version":"2"}//g' | sed '/^\s*$/d'

