#!/bin/bash

echoerr() { echo "$@" 1>&2; }

declare -a name=("cave" "release1-server" "release2-server" "development-server")

for i in "${name[@]}"
do
	ping_reply=$(ping -c 1 $i.skeen.info > /dev/null ; echo $?)

	if [ "$ping_reply" = "0" ]; then
		echo "$i ping is up"
	else
		echoerr "$i ping is down"
	fi
done
