#!/bin/bash

echoerr() { echo "$@" 1>&2; }

daemon_reply_expected="{\"error-message\":\"JSON Parse error on input = GET \/ HTTP\/1.1\",\"error-code\":\"GENERAL_SERVER_FAILURE\",\"version\":\"2\"}"

declare -a name=("cave" "release1-server" "release2-server" "development-server")

for i in "${name[@]}"
do
	daemon_reply=$(curl -s http://$i.skeen.info:37123/)

	if [ "$daemon_reply" = "$daemon_reply_expected" ]; then
		echo "$i daemon is up"
	else
		echoerr "$i daemon is down"
	fi
done
