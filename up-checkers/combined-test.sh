#!/bin/bash

cd /root/cave-utils/up-checkers/

LOG="/tmp/server_up.err"

./daemon-up.sh 2> $LOG
./ping-up.sh 2>> $LOG

if [[ -s $LOG ]] ; then
    cat mailheader.txt $LOG | /usr/sbin/sendmail sovende@gmail.com
fi
